/* 
 * File:   FileWriter.h
 * Author: tomas
 *
 * Created on 29. leden 2014, 16:51
 */

#ifndef FILEWRITER_H_456789876545678
#define	FILEWRITER_H_456789876545678

#include <iostream>
#include <fstream>

#include "Writer.h"

namespace doubrtom {
    
    class FileWriter : public doubrtom::Writer {
    public:
        FileWriter(const char * name, const char * path = NULL);
        ~FileWriter();
        
        Writer & write(const char * str);
    protected:
        std::ofstream file;
    };
    
}

#endif	/* FILEWRITER_H */

