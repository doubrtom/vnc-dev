/* 
 * File:   Logger.h
 * Author: tomas
 *
 * Created on January 24, 2014, 1:53 AM
 */

#ifndef LOGGER_H_3634472936482644
#define	LOGGER_H_3634472936482644

#include <ctime>
#include "Writer.h"
#include "../common/rdr/types.h"
#include "../common/rfb/Rect.h"

namespace doubrtom {
    
    class Logger {
    public:
        static Logger & getInstance();
        ~Logger();
        
        void setWriter(Writer * writer);
        double getTimeFromLastMessage();
        
        void logKey(rdr::U32 key, bool down);
        void logRequest();
        void logPointerEvent(const rfb::Point & pos, int buttonMask);
        void logMessage(const char * msg);
        void logMessage(); // log msg from buffer
        void logSecurityType(int type, bool selected);
        
        const char * getKeyName(int key);
        char * getBuffer();
    protected:
        static Logger logger;
        Writer * writer;
        char buffer[200];
        double lastMessageTime;
        
        char text[1000];
        char * nextChar;
        
        Logger();
    };
    
}

#endif	/* LOGGER_H */

