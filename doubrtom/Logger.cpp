
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <cmath>
#include <omp.h>

#include "Logger.h"

using namespace doubrtom;
using namespace std;

namespace doubrtom {
    const char * ASCII_TABLE[] = {" ", "!", "\"", "#", "$", "%", "&", 
        "'", "(", ")", "*", "+", ",", "-", ".", "/", "0", "1", "2", "3", "4", 
        "5", "6", "7", "8", "9", ":", ";", "<", "=", ">", "?", "@", "A", "B", 
        "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", 
        "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "[", "\\", "]", "^", 
        "_", "`", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", 
        "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", 
        "z", "{", "|", "}", "~"};
    const char * CONTROL_KEY[] = {"Backspace", "Tab", "Linefeed",
        "Clear", "Return", "Pause", "Scroll lock", "Sys req", "Escape"
        "Delete"};
    const char * MOTION_KEY[] = {"Home", "Left", "Up", "Right", "Down",
        "Page up", "Page down", "End", "Begin"};
    const char * MOUSE_BUTTON[] = {"Left", "Middle", "Rigth", "Wheel up", 
        "Wheel down"};
}

Logger Logger::logger;

Logger & Logger::getInstance() {
    return logger;
}

Logger::Logger() {
    lastMessageTime = -1;
    nextChar = text;
}

Logger::~Logger() {
    if (writer) delete writer;
}


void Logger::setWriter(Writer* writer) {
    this->writer = writer;
}

double Logger::getTimeFromLastMessage() {
    if (lastMessageTime < 0) lastMessageTime = omp_get_wtime();
    
    double now = omp_get_wtime();
    double diff = now - lastMessageTime;
    lastMessageTime = now;
    
    return diff;
}

void Logger::logKey(rdr::U32 key, bool down) {
    sprintf(buffer, "%lf: Posilam klavesu %s - %s\n", 
            getTimeFromLastMessage(), 
            getKeyName(key),
            (down ? "stisknuta" : "uvolnena"));
    writer->write(buffer);
    
    // print coherent text
    if (key >= 0x020 && key <= 0x07e && down) {
        *nextChar = ASCII_TABLE[key - 0x020][0];
        nextChar++;
    } else if (nextChar != text && down) {
        *nextChar = 0;
        writer->write("Napsany text: ").write(text).write("\n");
        nextChar = text;
    }
}

void Logger::logRequest() {
    sprintf(buffer, "%lf: Posilam request.\n", getTimeFromLastMessage());
    writer->write(buffer);
}

void Logger::logPointerEvent(const rfb::Point & pos, int buttonMask) {
    if (buttonMask == 0) return;
    
    int index = (int)log2(buttonMask);
    if (index > 4) sprintf(buffer, "Stisknuto tlacitko mysi: nezname\n");
    else sprintf(buffer, "Stisknuto tlacitko mysi: %s\n", MOUSE_BUTTON[index]);
    
    writer->write(buffer);
}

const char * Logger::getKeyName(int key) {
    const char * ret = 0;
    
    if (key >= 0x020 && key <= 0x07e) ret = ASCII_TABLE[key - 0x020];
//    if (key >= 0xFF08 && key <= 0xFFFF) return CONTROL_KEY[key - 0xFF08];
    if (key >= 0xFF50 && key <= 0xFF58) ret = MOTION_KEY[key - 0xFF50];
    
    return (ret == 0) ? "unknown" : ret;
}

void Logger::logMessage(const char * msg) {
    writer->write(msg);
}

void Logger::logMessage() {
    writer->write(buffer);
}

char * Logger::getBuffer() {
    return buffer;
}

void Logger::logSecurityType(int type, bool selected) {
    const char * typeName = NULL;
    
    // viz. RFB documentation page 9 - 10
    switch (type) {
        case 0: typeName = "Invalid"; break;
        case 1: typeName = "None"; break;
        case 2: typeName = "VNC Authentication"; break;
        case 5: typeName = "RA2"; break;
        case 6: typeName = "RA2ne"; break;
        case 16: typeName = "Tight"; break;
        case 17: typeName = "Ultra"; break;
        case 18: typeName = "TLS"; break;
        case 19: typeName = "VeNCrypt"; break;
        case 20: typeName = "GTK-VNC SASL"; break;
        case 21: typeName = "MD5 hash authentication"; break;
        case 22: typeName = "Colin Dean xvp"; break;
        default: typeName = "unknown";
    }
    
    if (selected) sprintf(buffer, "Vybrane zabezpeceni - %s\n", typeName);
    else sprintf(buffer, "Server nabizi zabezpeceni - %s\n", typeName);
    
    writer->write(buffer);
}

