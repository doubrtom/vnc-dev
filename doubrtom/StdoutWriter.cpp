#include <cstdio>
#include "Writer.h"
#include "StdoutWriter.h"

using namespace doubrtom;

doubrtom::Writer & StdoutWriter::write(const char* str) {
    std::printf("%s", str);
    return (*this);
}
