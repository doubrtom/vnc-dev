/* 
 * File:   Writer.h
 * Author: tomas
 *
 * Created on January 24, 2014, 2:01 AM
 */

#ifndef WRITER_H_35673898765467
#define	WRITER_H_35673898765467

namespace doubrtom {
 
    class Writer {
    public:
        virtual ~Writer() {}
        
        virtual Writer & write(const char * str) = 0;
    };
    
}

#endif	/* WRITER_H */

