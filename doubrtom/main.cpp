/* 
 * File:   main.cpp
 * Author: tomas
 *
 * Created on January 24, 2014, 7:38 AM
 */

#include <cstdlib>
#include "Logger.h"
#include "StdoutWriter.h"
#include "FileWriter.h"

using namespace std;
using namespace doubrtom;

/*
 * 
 */
int main(int argc, char** argv) {
    
//    Logger::getInstance().setWriter(new StdoutWriter());
    Logger::getInstance().setWriter(new FileWriter("vncviewer-log.txt"));
    Logger::getInstance().logKey(10, true);

    return 0;
}

