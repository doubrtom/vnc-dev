/* 
 * File:   StdoutWriter.h
 * Author: tomas
 *
 * Created on January 24, 2014, 2:04 AM
 */

#ifndef STDOUTWRITER_H_678390987
#define	STDOUTWRITER_H_678390987

#include "Writer.h"

namespace doubrtom {
    
    class StdoutWriter : public doubrtom::Writer {
    public:
        Writer & write(const char * str);
    };
    
}

#endif	/* STDOUTWRITER_H */

