#include <cstdio>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>

#include "FileWriter.h"

using namespace doubrtom;

FileWriter::FileWriter(const char * name, const char * path) {
    if (path == NULL) {
        path = getenv("HOME");
        if (!path) throw "Cannot create log file: environment variable $HOME not set.";
    }
    
    char filePath[strlen(path) + strlen(name) + 2];
    sprintf(filePath, "%s/%s", path, name);
    
    file.open(filePath);
    if (!file.is_open()) throw "File open error.";
}

FileWriter::~FileWriter() {
    file.close();
}

doubrtom::Writer & FileWriter::write(const char* str) {
    file << str;
    return (*this);
}

